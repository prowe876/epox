# Epox Scheduler #

The purpose of this application is to providing scheduling functionality only and be able to function in a distributed 
environment where appropriate load balancing and scheduling failover. This application is intended to both be deployed 
as a standalone service or be embedded as an library.

Right now there does not exist a dedicated fault tolerant open-source scheduling application. Most frameworks usually 
have their scheduler built in and have to implement their own fault-tolerant features. This initiative is to fill that 
gap so that people don�t reinvent the wheel and waste time implementing scheduler logic that already exists.

### Features ###
#### Configuration Features ####
- Configure schedules [x]
- Audit configurations [x]
- Configure/retrieve schedules/audit over rest [x]

#### Scheduling Features ####
- Kickoff scheduling [~]
- Notify a configurable target [~]
- Pause/resume schedules[~]
- Audit scheduling [~]

#### Authentication Service Features ####
- Integrate with Keycloak

#### Distributed Features ####
- Scheduling leader election
- Scheduling fail over
- Request load balancing
- Service discovery

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact