package org.duttydev.epox.epoxapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

/**
 * Created by pjrowe on 7/2/17.
 */
@Configuration
@EnableAspectJAutoProxy
public class EpoxConfiguration {

    @Bean(initMethod = "initialize")
    public ThreadPoolTaskScheduler taskScheduler() {
        return new ThreadPoolTaskScheduler();
    }
}
