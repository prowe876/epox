package org.duttydev.epox.epoxapi.domain.expression;

/**
 * Created by pjrowe on 7/24/17.
 */
public enum Month {
    JAN(1,31), FEB(2,28,29), MAR(3,31), APR(4,30), MAY(5,31), JUN(6,30), JUL(7,31), AUG(8,31), SEP(9,30), OCT(10,31),
    NOV(11,30), DEC(12,31), EVERY(-1,28);

    private final int value;
    private final int numOfDays;
    private final int altNumOfDays;

    private Month(int aValue,int aNumOfDays,int aAltNumOfDays) {
        value = aValue;
        numOfDays = aNumOfDays;
        altNumOfDays = aAltNumOfDays;
    }

    private Month(int aValue,int aNumOfDays) {
        value = aValue;
        numOfDays = aNumOfDays;
        altNumOfDays = numOfDays;
    }

    public int getValue() {
        return value;
    }

    public int getNumOfDays() {
        return numOfDays;
    }

    public int getAltNumOfDays() {
        return altNumOfDays;
    }


}
