package org.duttydev.epox.epoxapi.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by pjrowe on 7/23/17.
 */
@Configuration
@Import({springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration.class})
public class WebConfiguration {
}
