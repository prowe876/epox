package org.duttydev.epox.epoxapi.events.audit;

import org.duttydev.epox.epoxapi.persistence.repo.ConfigAuditEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class AuditEventPersistenceService {

    @Autowired
    private ConfigAuditEventRepository configAuditEventRepository;

    @EventListener
    public void persistConfigAuditEvent(ConfigAuditEvent aEvent) {
        configAuditEventRepository.save(aEvent);
    }

    @EventListener
    public void persistScheduleAuditEvent(ScheduleAuditEvent aEvent) {

    }
}
