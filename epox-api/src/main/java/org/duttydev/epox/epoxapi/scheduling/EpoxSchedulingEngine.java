package org.duttydev.epox.epoxapi.scheduling;

import org.duttydev.epox.epoxapi.events.audit.ConfigAuditEvent;
import org.duttydev.epox.epoxapi.events.audit.ConfigAuditEventType;
import org.duttydev.epox.epoxapi.events.audit.ScheduleAuditEvent;
import org.duttydev.epox.epoxapi.exceptions.UnsupportedConfigAuditEventException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.support.SimpleTriggerContext;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

@Service
public class EpoxSchedulingEngine {

    private final EpoxTaskScheduler scheduler;
    private final EpoxSchedulingErrorHandler errorHandler;

    private Map<String,EpoxSchedulingRecord> schedulingRecordStore = new HashMap<>();

    @Autowired
    public EpoxSchedulingEngine(EpoxTaskScheduler aScheduler,EpoxSchedulingErrorHandler aErrorHandler) {
        scheduler = aScheduler;
        errorHandler = aErrorHandler;
    }

    @EventListener
    public void handleConfigAuditEvents(ConfigAuditEvent aEvent) {

        switch(aEvent.getType()) {
            case SCHEDULE_CREATED: handleScheduleCreatedEvent(aEvent); break;
            case SCHEDULE_UPDATED: handleScheduleUpdatedEvent(aEvent); break;
            case SCHEDULE_DELETED: handleScheduleConfigDeletedEvent(aEvent); break;
            default:
                throw new UnsupportedConfigAuditEventException(aEvent.getType()+ " is not supported");
        }

    }

    @EventListener
    public void handleScheduleAuditEvents(ScheduleAuditEvent aEvent) {

    }

    private void handleScheduleCreatedEvent(ConfigAuditEvent aEvent) {
        if(aEvent.getType() == ConfigAuditEventType.SCHEDULE_CREATED) {
            EpoxTask task = new EpoxTask(aEvent.getEntityAfterEvent().getId(),
                    aEvent.getEntityAfterEvent().getSchedulableTargets(),
                    errorHandler);

            task.setExpression(aEvent.getEntityAfterEvent().getEpoxExpression());
            task.setTriggerContext(new SimpleTriggerContext());

            ScheduledFuture<?> future = scheduler.schedule(task,new EpoxTrigger(task.getExpression()));

            EpoxSchedulingRecord record = new EpoxSchedulingRecord();
            record.setScheduleId(task.getScheduleId());
            record.setTask(task);
            record.setScheduledFuture(future);

            schedulingRecordStore.put(record.getScheduleId(),record);
        }
    }

    private void handleScheduleUpdatedEvent(ConfigAuditEvent aEvent) {
        if(aEvent.getType() == ConfigAuditEventType.SCHEDULE_UPDATED) {

            EpoxSchedulingRecord record = schedulingRecordStore.get(aEvent.getEntityAfterEvent().getId());

            boolean rescheduleTask = false;

            // if expression is different, reschedule task
            if( !aEvent.getEntityAfterEvent().getEpoxExpression().equals(record.getTask().getExpression()) ) {
                record.getScheduledFuture().cancel(false);
                record.getTask().setExpression(aEvent.getEntityAfterEvent().getEpoxExpression());
                rescheduleTask = true;
            }

            // if targets is different, reschedule task
            if( !aEvent.getEntityAfterEvent().getSchedulableTargets().equals(record.getTask().getSchedulableTargets()) ) {
                record.getScheduledFuture().cancel(false);
                EpoxTask task = new EpoxTask(aEvent.getEntityAfterEvent().getId(),
                        aEvent.getEntityAfterEvent().getSchedulableTargets(),
                        errorHandler);
                record.setTask(task);
                rescheduleTask = true;
            }

            if(rescheduleTask) {
                ScheduledFuture<?> future = scheduler.schedule(record.getTask(),
                        new EpoxTrigger(record.getTask().getExpression()));
                record.setScheduledFuture(future);
            }
        }
    }

    private void handleScheduleConfigDeletedEvent(ConfigAuditEvent aEvent) {
        if(aEvent.getType() == ConfigAuditEventType.SCHEDULE_DELETED) {
            EpoxSchedulingRecord record = schedulingRecordStore.remove(aEvent.getEntityAfterEvent().getId());
            record.getScheduledFuture().cancel(false);
        }
    }

}
