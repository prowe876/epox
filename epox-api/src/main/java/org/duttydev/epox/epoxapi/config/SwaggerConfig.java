package org.duttydev.epox.epoxapi.config;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by pjrowe on 6/28/17.
 */
@Controller
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket scheduleApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .paths(PathSelectors.ant("/api/**"))
                .build();
    }

    @Bean
    public ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Epox Scheduler API")
                .description("The purpose of this API is to providing scheduling functionality only and be able to\n " +
                        "function in a distributed environment where appropriate load balancing and scheduling\n " +
                        "failover. This application is intended to both be deployed as a standalone service or be\n " +
                        "embedded as an library. Right now there doesn’t exist a dedicated fault tolerant open-source\n " +
                        "scheduling application. Most frameworks usually have their scheduler built in and have to\n " +
                        "implement their own fault-tolerant features. This initiative is to fill that gap so that\n " +
                        "people don’t reinvent the wheel and waste time implementing scheduler logic that\n " +
                        "already exists.\n")
                .contact(new Contact("Peter-John Rowe","rowepj.com","rowe.peterjohn@gmail.com"))
                .license("MIT")
               // .licenseUrl("")
                .version("0.0.1")
                .build();
    }

    private Predicate<String> schedulesPath() {
        return PathSelectors.ant("/schedules/**");
    }

}
