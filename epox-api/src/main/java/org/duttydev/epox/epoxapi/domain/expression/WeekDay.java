package org.duttydev.epox.epoxapi.domain.expression;

/**
 * Created by pjrowe on 7/24/17.
 */
public enum WeekDay {
    SUN(1), MON(2), TUE(3), WED(4), THU(5), FRI(6), SAT(7), EVERY(-1);

    private final int value;

    private WeekDay(int aValue) {
        value = aValue;
    }

    public int getValue() {
        return value;
    }
}
