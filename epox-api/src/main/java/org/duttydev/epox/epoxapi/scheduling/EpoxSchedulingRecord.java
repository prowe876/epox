package org.duttydev.epox.epoxapi.scheduling;

import org.duttydev.epox.epoxapi.domain.targets.SchedulableTarget;
import org.springframework.scheduling.TriggerContext;

import java.util.List;
import java.util.concurrent.ScheduledFuture;

public class EpoxSchedulingRecord {

    private String scheduleId;
    private EpoxTask task;
    private ScheduledFuture<?> scheduledFuture;

    public String getScheduleId() {
        return scheduleId;
    }

    public EpoxTask getTask() {
        return task;
    }

    public void setTask(EpoxTask task) {
        this.task = task;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public ScheduledFuture<?> getScheduledFuture() {
        return scheduledFuture;
    }

    public void setScheduledFuture(ScheduledFuture<?> scheduledFuture) {
        this.scheduledFuture = scheduledFuture;
    }

}
