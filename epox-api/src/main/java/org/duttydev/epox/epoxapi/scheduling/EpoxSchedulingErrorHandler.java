package org.duttydev.epox.epoxapi.scheduling;

import org.springframework.stereotype.Component;
import org.springframework.util.ErrorHandler;

@Component
public class EpoxSchedulingErrorHandler implements ErrorHandler {
    @Override
    public void handleError(Throwable throwable) {

    }
}
