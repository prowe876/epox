package org.duttydev.epox.epoxapi.events.audit;

public enum ScheduleAuditEventType {
    SCHEDULE_STARTED, SCHEDULE_REACHED, SCHEDULE_TRIGGERED_TARGET, SCHEDULE_TRIGGERED_TARGET_FAILED, SCHEDULE_RECURRED
}
