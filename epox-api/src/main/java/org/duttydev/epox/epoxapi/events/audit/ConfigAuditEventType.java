package org.duttydev.epox.epoxapi.events.audit;

public enum ConfigAuditEventType {
    SCHEDULE_CREATED,SCHEDULE_UPDATED,SCHEDULE_DELETED
}
