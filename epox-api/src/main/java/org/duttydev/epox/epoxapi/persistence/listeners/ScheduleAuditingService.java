package org.duttydev.epox.epoxapi.persistence.listeners;

import org.duttydev.epox.epoxapi.domain.Schedule;
import org.duttydev.epox.epoxapi.events.audit.*;
import org.duttydev.epox.epoxapi.persistence.repo.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.mapping.event.*;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ScheduleAuditingService extends AbstractMongoEventListener<Schedule> {

    private final ScheduleRepository repo;
    private final AuditEventPublisher publisher;

    private Map<String,Schedule> scheduleBeforeSaveEventStore = new ConcurrentHashMap<>();
    private Map<String,Schedule> scheduleBeforeDeleteEventStore = new ConcurrentHashMap<>();


    @Autowired
    public ScheduleAuditingService(ScheduleRepository aRepo, AuditEventPublisher aAuditEventPublisher) {
        repo = aRepo;
        publisher = aAuditEventPublisher;
    }

    @Override
    public void onBeforeSave(BeforeSaveEvent<Schedule> event) {
        String scheduleId = event.getSource().getId();
        if(scheduleId != null) {
            Schedule oldSchedule = repo.findOne(scheduleId);
            scheduleBeforeSaveEventStore.put(scheduleId,oldSchedule);
        }
    }

    @Override
    public void onAfterSave(AfterSaveEvent<Schedule> event) {
        String scheduleId = event.getSource().getId();
        Optional<Schedule> before  = Optional.ofNullable(scheduleBeforeSaveEventStore.remove(scheduleId));

        AuditEvent auditEvent = new ConfigAuditEvent("s",
                before.isPresent() ? ConfigAuditEventType.SCHEDULE_UPDATED : ConfigAuditEventType.SCHEDULE_CREATED,
                before.orElse(null),
                event.getSource(),
                "current-user TBD");

        publisher.publishAuditEvent(auditEvent);
    }

    @Override
    public void onBeforeDelete(BeforeDeleteEvent<Schedule> event) {
        String scheduleId = (String) event.getSource().get("id");
        if(scheduleId != null) {
            Schedule scheduleToBeDeleted = repo.findOne(scheduleId);
            scheduleBeforeDeleteEventStore.put(scheduleId,scheduleToBeDeleted);
        }
    }

    @Override
    public void onAfterDelete(AfterDeleteEvent<Schedule> event) {
        String scheduleId = (String) event.getSource().get("id");
        Schedule deletedSchedule = scheduleBeforeDeleteEventStore.get(scheduleId);

        AuditEvent auditEvent = new ConfigAuditEvent("s",
                ConfigAuditEventType.SCHEDULE_DELETED,
                deletedSchedule,
                null,
                "current-user TBD");

        publisher.publishAuditEvent(auditEvent);
    }
}
