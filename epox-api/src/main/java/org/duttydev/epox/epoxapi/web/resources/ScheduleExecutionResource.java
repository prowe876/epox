package org.duttydev.epox.epoxapi.web.resources;

import org.duttydev.epox.epoxapi.domain.Action;
import org.duttydev.epox.epoxapi.domain.Schedule;
import org.duttydev.epox.epoxapi.domain.ScheduleExecution;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by pjrowe on 6/29/17.
 */
//@RestController
@RequestMapping(path = "/api/schedule-executions",produces = MediaType.APPLICATION_JSON_VALUE)
public class ScheduleExecutionResource {
    @GetMapping
    public ResponseEntity<Page<ScheduleExecution>> getScheduleExecutions(
            @RequestParam(value = "page",defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "0") Integer size) {
        return ResponseEntity.ok().build();
    }
}
