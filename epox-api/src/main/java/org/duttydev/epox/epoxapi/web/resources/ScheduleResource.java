package org.duttydev.epox.epoxapi.web.resources;

import org.duttydev.epox.epoxapi.domain.Schedule;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.concurrent.*;

/**
 * Created by pjrowe on 6/28/17.
 */

//@RestController
@RequestMapping(path = "/api/schedules",produces = MediaType.APPLICATION_JSON_VALUE)
public class ScheduleResource {

    @GetMapping
    public ResponseEntity<Page<Schedule>> getSchedules(
            @RequestParam(value = "page",defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "0") Integer size) {
        return ResponseEntity.ok().build();
    }

    @PostMapping
    public ResponseEntity<Schedule> createSchedule(@RequestBody Schedule newSchedule) {
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Schedule> getSchedule(@PathVariable("id") Long id) {
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteSchedule(@PathVariable("id") Long id) {
        return ResponseEntity.ok().build();
    }

    @PostMapping(path = "/{id}/pause")
    public ResponseEntity<Void> pauseSchedule(@PathVariable("id") Long id) {
        return ResponseEntity.ok().build();
    }

    @PostMapping(path = "/{id}/resume")
    public ResponseEntity<Void> resumeSchedule(@PathVariable("id") Long id) {
        return ResponseEntity.ok().build();
    }

    static class EpoxScheduledThreadPoolExecutor extends ScheduledThreadPoolExecutor {
        public EpoxScheduledThreadPoolExecutor(int corePoolSize, ThreadFactory threadFactory, RejectedExecutionHandler handler) {
            super(corePoolSize, threadFactory, handler);
        }

        @Override
        public ScheduledFuture<?> schedule(Runnable command,
                                           long delay,
                                           TimeUnit unit) {

            ScheduledFuture<?> f = super.schedule(command,delay,unit);
            System.out.println("scheduling begins...");
            return f;
        }
    }

    static class EpoxThreadPoolTaskScheduler extends ThreadPoolTaskScheduler {

        @Override
        protected ScheduledExecutorService createExecutor(int poolSize, ThreadFactory threadFactory, RejectedExecutionHandler rejectedExecutionHandler) {
            return new EpoxScheduledThreadPoolExecutor(poolSize, threadFactory, rejectedExecutionHandler);
        }
    }

    static class Targeter implements Runnable {

        int runCount = 0;

        @Override
        public void run() {

        }
    }

    public static void main(String[] args) throws Exception {
        ThreadPoolTaskScheduler scheduler = new EpoxThreadPoolTaskScheduler();
        scheduler.initialize();

        ScheduledFuture f  = scheduler.schedule(
                () -> {
                    System.out.println(Instant.now().getEpochSecond());
                },
                new CronTrigger("*/1 * * * * *"));
        TimeUnit.SECONDS.sleep(5);
        System.out.println("cancel succes: "+f.cancel(true));
        System.out.println("cancelling task");
        scheduler.shutdown();
    }

}
