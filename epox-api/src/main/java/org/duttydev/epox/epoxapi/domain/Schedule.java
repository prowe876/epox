package org.duttydev.epox.epoxapi.domain;

import org.duttydev.epox.epoxapi.domain.expression.EpoxExpression;
import org.duttydev.epox.epoxapi.domain.targets.SchedulableTarget;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by pjrowe on 6/28/17.
 */
@Document
public class Schedule {

    @Id
    private String id;
    private String name;
    private EpoxExpression epoxEpression;
    private boolean isActive;
    private List<SchedulableTarget> schedulableTargets;

    public Schedule() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EpoxExpression getEpoxExpression() {
        return epoxEpression;
    }

    public void setEpoxExpression(EpoxExpression expression) {
        this.epoxEpression = expression;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public List<SchedulableTarget> getSchedulableTargets() {
        return schedulableTargets;
    }

    public void setSchedulableTargets(List<SchedulableTarget> schedulableTargets) {
        this.schedulableTargets = schedulableTargets;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", epoxEpression='" + epoxEpression + '\'' +
                ", isActive=" + isActive +
                ", schedulableTargets=" + schedulableTargets +
                '}';
    }
}
