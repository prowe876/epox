package org.duttydev.epox.epoxapi.exceptions;

public class UnsupportedConfigAuditEventException extends RuntimeException {

    public UnsupportedConfigAuditEventException(String aMessage) {
        super(aMessage);
    }
}
