package org.duttydev.epox.epoxapi.events.audit;

import org.springframework.context.ApplicationEvent;
import org.springframework.data.annotation.Id;

import java.time.Instant;

public abstract class AuditEvent extends ApplicationEvent {

    @Id
    private String id;

    public AuditEvent(Object source) {
        super(source);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
