package org.duttydev.epox.epoxapi.domain.targets;

import org.duttydev.epox.epoxapi.domain.TargetException;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by pjrowe on 6/29/17.
 */
public abstract class SchedulableTarget implements Serializable {

    private String id;
    private String name;

    public abstract void initialise(Map<String,Object> targetConfig) throws TargetException;
    public abstract void execute() throws TargetException;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
