package org.duttydev.epox.epoxapi.events.audit;

import org.duttydev.epox.epoxapi.domain.Schedule;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Document
public class ConfigAuditEvent extends AuditEvent {

    private  ConfigAuditEventType type;
    private  Schedule entityBeforeEvent;
    private  Schedule entityAfterEvent;
    private  String actorName;

    public ConfigAuditEvent() {
        super("a");
    }

    public ConfigAuditEvent(Object source,ConfigAuditEventType aType,
                            Schedule aEntityBeforeEvent, Schedule aEntityAfterEvent,
                            String aActorName) {
        super(source);
        type = aType;
        entityBeforeEvent = aEntityBeforeEvent;
        entityAfterEvent = aEntityAfterEvent;
        actorName = aActorName;
    }

    public ConfigAuditEventType getType() {
        return type;
    }

    public Schedule getEntityBeforeEvent() {
        return entityBeforeEvent;
    }

    public Schedule getEntityAfterEvent() {
        return entityAfterEvent;
    }

    public String getActorName() {
        return actorName;
    }

    public void setType(ConfigAuditEventType type) {
        this.type = type;
    }

    public void setEntityBeforeEvent(Schedule entityBeforeEvent) {
        this.entityBeforeEvent = entityBeforeEvent;
    }

    public void setEntityAfterEvent(Schedule entityAfterEvent) {
        this.entityAfterEvent = entityAfterEvent;
    }

    public void setActorName(String actorName) {
        this.actorName = actorName;
    }
}
