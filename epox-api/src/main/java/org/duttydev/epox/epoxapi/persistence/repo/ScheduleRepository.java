package org.duttydev.epox.epoxapi.persistence.repo;

import org.duttydev.epox.epoxapi.domain.Schedule;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by pjrowe on 7/23/17.
 */
@RepositoryRestResource(collectionResourceRel = "schedules", path = "schedules")
public interface ScheduleRepository extends PagingAndSortingRepository<Schedule,String> {

}
