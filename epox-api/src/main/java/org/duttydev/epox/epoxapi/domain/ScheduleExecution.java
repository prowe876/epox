package org.duttydev.epox.epoxapi.domain;

/**
 * Created by pjrowe on 6/29/17.
 */
public class ScheduleExecution {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
