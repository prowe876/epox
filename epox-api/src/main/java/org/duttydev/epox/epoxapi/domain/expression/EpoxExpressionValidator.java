package org.duttydev.epox.epoxapi.domain.expression;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.util.Assert;

import java.time.DateTimeException;
import java.time.Year;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by pjrowe on 7/24/17.
 */
public class EpoxExpressionValidator {

    public static void validate(EpoxExpression expression) throws EpoxExpressionException {
        if (expression == null) {
            throw new EpoxExpressionException("Epox scheduler cannot be null");
        }

        validateMinutes(expression);
        validateHours(expression);
        validateMonths(expression);
        validateYears(expression);
        validateWeekDaysAndDayOfMonths(expression);
    }

    /**
     * Validate the minutes portion of the epox expression.
     * Valid values are 0 - 59 and -1.
     *
     * @param expression
     * @throws EpoxExpressionException
     */
    private static void validateMinutes(EpoxExpression expression) throws EpoxExpressionException {
        if (ArrayUtils.isEmpty(expression.getMinutes())) {
            throw new EpoxExpressionException("The minutes portion of the epox expression cannot be null or empty");
        }

        int[] invalidMinutes = Arrays.stream(expression.getMinutes())
                .filter(minute -> minute < -1 || minute > 59)
                .toArray();

        if (invalidMinutes.length > 0) {
            throw new EpoxExpressionException("The following minutes are invalid: " + Arrays.toString(invalidMinutes));
        }
    }

    /**
     * Validate the hours portion of the epox expression.
     * Valid values are 0 - 23 and -1.
     *
     * @param expression
     * @throws EpoxExpressionException
     */
    private static void validateHours(EpoxExpression expression) throws EpoxExpressionException {
        if (ArrayUtils.isEmpty(expression.getHours())) {
            throw new EpoxExpressionException("The hours portion of the epox expression cannot be null or empty");
        }

        int[] invalidHours = Arrays.stream(expression.getHours())
                .filter(hour -> hour < -1 || hour > 23)
                .toArray();

        if (invalidHours.length > 0) {
            throw new EpoxExpressionException("The following hours are invalid: " + Arrays.toString(invalidHours));
        }
    }

    /**
     * Validate the months array of the expression.
     * Months array cannot be empty or null.
     *
     * @param expression
     * @throws EpoxExpressionException
     */
    private static void validateMonths(EpoxExpression expression) throws EpoxExpressionException {
        if (ArrayUtils.isEmpty(expression.getMonths())) {
            throw new EpoxExpressionException("The months portion of the cannot be null or empty");
        }
    }

    /**
     * Validates the years array of the expression.
     * Years array cannot be null or empty. Valid years from -1 up.
     *
     * @param expression
     * @throws EpoxExpressionException
     */
    private static void validateYears(EpoxExpression expression) throws EpoxExpressionException {
        if (ArrayUtils.isEmpty(expression.getYears())) {
            throw new EpoxExpressionException("The years portion of the cannot be null or empty");
        }
    }

    /**
     * TODO
     * Validates both days of month and week days.
     * Only one can be null or empty at a time.
     *
     * @param expression
     */
    private static void validateWeekDaysAndDayOfMonths(EpoxExpression expression) throws EpoxExpressionException {

        if (ArrayUtils.isNotEmpty(expression.getWeekDays())) {
            if (ArrayUtils.isNotEmpty(expression.getDayOfMonths())) {
                throw new EpoxExpressionException("Both weekdays and days of months cannot both be used. Select one");
            }
        } else if (ArrayUtils.isNotEmpty(expression.getDayOfMonths())) {
            StringBuilder builder = new StringBuilder();
            final int ordinaryYear = 2001;
            final int leapYear = 2000;

            // min month
            Month month = Arrays.stream(expression.getMonths())
                    .min(Comparator.comparing(Month::getValue))
                    .get();

            if( ArrayUtils.contains(expression.getYears(),-1) ) {
                // least # of days in a month
            } else {
                for( int year: expression.getYears() ) {
                    try {
                        YearMonth.of(year, month.getValue());
                    } catch (DateTimeException exception) {
                        builder.append(exception.getMessage());
                        builder.append(". ");
                    }
                }
            }

            String message = builder.toString();
            if(!message.isEmpty()) {
                throw new EpoxExpressionException(message);
            }

        } else {
            throw new EpoxExpressionException("Both week days and days of month cannot null or empty. " +
                    "One of them must be valid");
        }
    }

}
