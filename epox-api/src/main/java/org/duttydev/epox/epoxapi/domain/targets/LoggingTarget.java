package org.duttydev.epox.epoxapi.domain.targets;

import org.apache.tomcat.jni.Local;
import org.duttydev.epox.epoxapi.domain.TargetException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Created by pjrowe on 7/20/17.
 */
public class LoggingTarget extends LocalTarget {
    private static final Logger LOG = LoggerFactory.getLogger(LoggingTarget.class);

    public LoggingTarget() {

    }

    @Override
    public void initialise(Map<String, Object> targetConfig) throws TargetException {
        // DO NOTHING
    }

    @Override
    public void execute() throws TargetException {
        LOG.info("Target was triggered");
    }
}
