package org.duttydev.epox.epoxapi.events.audit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class AuditEventPublisher {

    private final ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    public AuditEventPublisher(ApplicationEventPublisher aPublisher) {
        applicationEventPublisher = aPublisher;
    }

    public void publishAuditEvent(AuditEvent event) {
        applicationEventPublisher.publishEvent(event);
    }

}
