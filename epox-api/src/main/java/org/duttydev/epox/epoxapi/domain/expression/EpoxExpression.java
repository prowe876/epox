package org.duttydev.epox.epoxapi.domain.expression;

import java.util.Arrays;

/**
 * Created by pjrowe on 7/24/17.
 */

/**
 * Expression for schedule that is equivalent to a cron expression.
 * This expression allows for not only recurring schedules but one
 * time schedules as well.
 */
public class EpoxExpression {

    /**
     * The array of minutes that a schedule will be triggered for.
     * Valid values are 0 - 59, and -1 for every minute.
     * If -1 is present in the array, it will take precedence
     * over the other values.
     */
    private int[] minutes;

    /**
     * The array of hours that a schedule will be triggered for.
     * Valid values are 0 - 23, and -1 for every hour.
     * If -1 is present in the array, it will take precedence
     * over the other values.
     */
    private int[] hours;

    /**
     * The array of dayOfMonths that a schedule will be triggered for.
     * Valid values are 1 - 31, and  -1 for every hour.
     * If -1 is present in the array, it will take precedence
     * over the other values. The following values are valid for months:
     *
     * FEB - [ 1 - 28 ]
     * APR, JUN, SEP, NOV - [ 1 - 30 ]
     * JAN, MAR, MAY, JUL, AUG, OCT, DEC [ 1 - 31 ]
     */
    private int[] dayOfMonths;

    /**
     * The array of WeekDays that a schedule will be triggered for.
     * If EVERY is present in the array, it will take precedence
     * over the other values.
     */
    private WeekDay[] weekDays;

    /**
     * The array of Months that a schedule will be triggered for.
     * If EVERY is present in the array, it will take precedence
     * over the other values.
     */
    private Month[] months;

    /**
     * The array of years that a schedule will be triggered for.
     * If EVERY is present in the array, it will take precedence
     * over the other values.
     */
    private int[] years;

    public int[] getMinutes() {
        return minutes;
    }

    public void setMinutes(int[] minutes) {
        this.minutes = minutes;
    }

    public int[] getHours() {
        return hours;
    }

    public void setHours(int[] hours) {
        this.hours = hours;
    }

    public int[] getDayOfMonths() {
        return dayOfMonths;
    }

    public void setDayOfMonths(int[] dayOfMonths) {
        this.dayOfMonths = dayOfMonths;
    }

    public WeekDay[] getWeekDays() {
        return weekDays;
    }

    public void setWeekDays(WeekDay[] weekDays) {
        this.weekDays = weekDays;
    }

    public Month[] getMonths() {
        return months;
    }

    public void setMonths(Month[] months) {
        this.months = months;
    }

    public int[] getYears() {
        return years;
    }

    public void setYears(int[] years) {
        this.years = years;
    }

    @Override
    public String toString() {
        return "EpoxExpression{" +
                "minutes=" + Arrays.toString(minutes) +
                ", hours=" + Arrays.toString(hours) +
                ", dayOfMonth=" + Arrays.toString(dayOfMonths) +
                ", weekDays=" + Arrays.toString(weekDays) +
                ", months=" + Arrays.toString(months) +
                ", years=" + Arrays.toString(years) +
                '}';
    }
}
