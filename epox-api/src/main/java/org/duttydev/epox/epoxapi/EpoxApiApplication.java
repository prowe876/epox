package org.duttydev.epox.epoxapi;

import org.duttydev.epox.epoxapi.domain.Schedule;
import org.duttydev.epox.epoxapi.domain.targets.LoggingTarget;
import org.duttydev.epox.epoxapi.persistence.repo.ScheduleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.UUID;

@SpringBootApplication
public class EpoxApiApplication implements CommandLineRunner {

	private static final Logger LOG = LoggerFactory.getLogger(EpoxApiApplication.class);

	@Autowired
	private ScheduleRepository repo;

	public static void main(String[] args) {
		SpringApplication.run(EpoxApiApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		Schedule sched = new Schedule();
		sched.setName("First Schedule");
		sched.setActive(true);

		LoggingTarget target = new LoggingTarget();
		target.setId(UUID.randomUUID().toString());
		target.setName("Logging Target");
		sched.setSchedulableTargets(Arrays.asList(target));

		Schedule schedule = repo.save(sched);
		LOG.info(schedule.toString());
	}
}
