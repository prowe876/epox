package org.duttydev.epox.epoxapi.scheduling;

import org.duttydev.epox.epoxapi.domain.expression.EpoxExpression;
import org.duttydev.epox.epoxapi.domain.targets.SchedulableTarget;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.support.DelegatingErrorHandlingRunnable;
import org.springframework.util.ErrorHandler;

import java.util.List;

public class EpoxTask extends DelegatingErrorHandlingRunnable {

    private final String scheduleId;
    private EpoxExpression expression;
    private TriggerContext triggerContext;
    private final List<SchedulableTarget> schedulableTargets;

    public EpoxTask(String aScheduleId,List<SchedulableTarget> aSchedulableTargets, ErrorHandler errorHandler) {
        super(() -> aSchedulableTargets.forEach(SchedulableTarget::execute), errorHandler);
        scheduleId = aScheduleId;
        schedulableTargets =aSchedulableTargets;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public EpoxExpression getExpression() {
        return expression;
    }

    public void setExpression(EpoxExpression expression) {
        this.expression = expression;
    }

    public TriggerContext getTriggerContext() {
        return triggerContext;
    }

    public void setTriggerContext(TriggerContext triggerContext) {
        this.triggerContext = triggerContext;
    }

    public List<SchedulableTarget> getSchedulableTargets() {
        return schedulableTargets;
    }
}
