package org.duttydev.epox.epoxapi.domain.expression;

/**
 * Created by pjrowe on 7/24/17.
 */
public class EpoxExpressionException extends Exception {

    public EpoxExpressionException() {
        super();
    }

    public EpoxExpressionException(String message) {
        super(message);
    }

    public EpoxExpressionException(Throwable throwable) {
        super(throwable);
    }

    public EpoxExpressionException(String message,Throwable throwable) {
        super(message,throwable);
    }
}
