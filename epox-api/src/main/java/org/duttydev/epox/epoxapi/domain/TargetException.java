package org.duttydev.epox.epoxapi.domain;

import net.bytebuddy.implementation.bytecode.Throw;

import java.lang.annotation.Target;

/**
 * Created by pjrowe on 7/20/17.
 */
public class TargetException extends RuntimeException {

    public TargetException() {
        super();
    }

    public TargetException(String message) {
        super(message);
    }

    public TargetException(Throwable throwable) {
        super(throwable);
    }

    public TargetException(String message,Throwable throwable) {
        super(message,throwable);
    }
}
