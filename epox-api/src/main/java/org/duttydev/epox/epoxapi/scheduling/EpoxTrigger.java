package org.duttydev.epox.epoxapi.scheduling;

import org.duttydev.epox.epoxapi.domain.expression.EpoxExpression;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;

import java.util.Date;

public class EpoxTrigger implements Trigger {

    private final EpoxExpression epoxExpression;

    public EpoxTrigger(EpoxExpression aEpoxExpression) {
        epoxExpression = aEpoxExpression;
    }

    @Override
    public Date nextExecutionTime(TriggerContext triggerContext) {

        // TODO
        return null;
    }
}
