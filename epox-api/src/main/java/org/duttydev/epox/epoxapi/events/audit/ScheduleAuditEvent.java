package org.duttydev.epox.epoxapi.events.audit;

import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Document
public class ScheduleAuditEvent extends AuditEvent {

    private final String scheduleId;
    private final ScheduleAuditEventType type;

    public ScheduleAuditEvent(Object source, String aScheduleId, ScheduleAuditEventType aType) {
        super(source);
        scheduleId = aScheduleId;
        type = aType;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public ScheduleAuditEventType getType() {
        return type;
    }
}
