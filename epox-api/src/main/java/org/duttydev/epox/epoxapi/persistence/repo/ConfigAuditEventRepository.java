package org.duttydev.epox.epoxapi.persistence.repo;

import org.duttydev.epox.epoxapi.events.audit.ConfigAuditEvent;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "config-audit", path = "audit-configs")
public interface ConfigAuditEventRepository extends PagingAndSortingRepository<ConfigAuditEvent,String>{
}
