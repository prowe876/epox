package org.duttydev.epox.epoxapi.web.resources;

import org.duttydev.epox.epoxapi.domain.targets.SchedulableTarget;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by pjrowe on 6/29/17.
 */
//@RestController
@RequestMapping(path = "/api/targets",produces = MediaType.APPLICATION_JSON_VALUE)
public class TargetResource {

    @GetMapping
    public ResponseEntity<Page<SchedulableTarget>> getTargets(
            @RequestParam(value = "page",defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "0") Integer size) {
        return ResponseEntity.ok().build();
    }

    @PostMapping
    public ResponseEntity<SchedulableTarget> createTarget(@RequestBody SchedulableTarget newSchedule) {
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<SchedulableTarget> getTarget(@PathVariable("id") Long id) {
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteTarget(@PathVariable("id") Long id) {
        return ResponseEntity.ok().build();
    }
}
