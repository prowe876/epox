package org.duttydev.epox.epoxserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EpoxServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EpoxServerApplication.class, args);
	}
}
